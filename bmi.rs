use std::env;

fn main() {

	let args: Vec<String> = env::args().collect();
	if args.len() < 3 {
		println!("You need to provide two arguments: height and weight.");
		return;
	}

    let h_p = args[1].parse::<u16>();
    if h_p.is_err() {
    	println!("Cannot parse height as a number. Got {:?}", args[1]);
    	return;
    }
    let h = h_p.unwrap();

    let w_p = args[2].parse::<u16>();
    if w_p.is_err() {
    	println!("Cannot parse weight as a number. Got {:?}", args[2]);
    	return;
    }
    let w = w_p.unwrap();

    let bmi = w as f64 / ((h as f64/100.0 * (h as f64/100.0)));
    let desc = match bmi as u64 {
    	0 ..= 19 => "UNDERWEIGHT",
    	0 ..= 24 => "NORMAL",
    	_ => "OVERWEIGHT",
    };
    println!("BMI {} {} = {:0.1} ({})", h, w, bmi, desc);
}

