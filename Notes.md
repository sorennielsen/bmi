# Notes

## AWS

docker run --rm amazon/aws-cli
docker pull amazon/aws-cli:latest

## Apply Pod:
kubectl apply -f bmi-server.json

## Delete Pod:
kubectl delete -f bmi-server.json

## Create secret for pulling images from private repo

DOCKER_REGISTRY_SERVER=docker.io
DOCKER_USER=sorennielsen
DOCKER_EMAIL=contact@cph.dev
DOCKER_PASSWORD=

kubectl create secret docker-registry docker-hub-secret \
  --docker-server=$DOCKER_REGISTRY_SERVER \
  --docker-username=$DOCKER_USER \
  --docker-password=$DOCKER_PASSWORD \
  --docker-email=$DOCKER_EMAIL

## Let the default account be able to pull images from Docker Hub

kubectl edit serviceaccount default

Add:
  imagePullSecrets:
  - name: docker-hub-secret


## Quick deployment

kubectl run bmi \
--image=sorennielsen/bmi:latest-arm64 \
--replicas=3 \
--port=8080 \
--labels="app=bmi,env=test" \
-- serve

## Deployment manifest

kubectl apply -f bmi-deployment.json --record

## Expose deployment as a service

kubectl expose deployment bmi

or

kubectl apply -f bmi-service.json

## Rolling restart of deployment

kubectl rollout restart deployment bmi

## Watch stuff

kubectl get endpoints bmi --watch

watch --no-title -c \
  "echo '${bold}${underline}SERVICES${normal}' ; kubectl get services -o wide ; echo '' ; \
   echo '${bold}${underline}DEPLOYMENTS${normal}' ; kubectl get deployments.apps -o wide ; echo '' ; \
   echo '${bold}${underline}PODS${normal}' ; kubectl get pods -o wide ; echo '' ; \
   echo '${bold}${underline}NODES${normal}' ; kubectl get nodes -o wide \
  "

## GitLab CI/CD

docker login registry.gitlab.com
docker build -t registry.gitlab.com/sorennielsen/cph-wtf .
docker push registry.gitlab.com/sorennielsen/cph-wtf
